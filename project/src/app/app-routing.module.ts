import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CardComponent } from './card/card.component';
import { GenerationComponent } from './generation/generation.component';
import { LoginComponent } from './login/login.component';
import { HistoryComponent } from './history/history.component';

const routes: Routes = [
  { path: 'card', component: CardComponent },
  { path: 'generation', component: GenerationComponent },
  { path: 'login', component: LoginComponent },
  { path: 'history', component: HistoryComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
