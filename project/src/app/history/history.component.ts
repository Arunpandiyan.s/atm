import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.css'],
})
export class HistoryComponent implements OnInit {
  todo: any;
  constructor(private http: HttpClient) {}
  ngOnInit() {
    this.http
      .get('https://http-d6945-default-rtdb.firebaseio.com/Bank.json')
      .subscribe((response: any) => {
        // console.table(data);
        this.todo = response;
      });
  }
}
