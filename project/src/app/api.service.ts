import { Inject, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Card } from './card.model';
@Injectable({
  providedIn: 'root',
})
export class ApiService {
  cardDetails: Card;
  constructor(private http: HttpClient) {}
  get(url: string) {
    return this.http.get(url);
  }
  saveCardDetails(card: Card) {
    this.cardDetails = card;
  }
  getCardDetails() {
    return this.cardDetails;
  }
}
