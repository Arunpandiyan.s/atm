import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-generation',
  templateUrl: './generation.component.html',
  styleUrls: ['./generation.component.css'],
})
export class GenerationComponent implements OnInit {
  inputForm: FormGroup;
  constructor(
    private cardGenerateService: ApiService,
    private router: Router
  ) {}
  ngOnInit() {
    this.inputForm = new FormGroup({
      cardNo: new FormControl('', [Validators.required]),
      cvv: new FormControl('', [Validators.required, Validators.minLength(3)]),
      month: new FormControl('', [Validators.required]),
      year: new FormControl('', [Validators.required]),
      avlBalance: new FormControl('', [Validators.required]),
    });
  }
  onFormSubmit() {
    this.cardGenerateService.saveCardDetails(this.inputForm.value);
    this.router.navigate(['./card']);
    console.log('Submitted values: ', this.inputForm.value);
  }
}
