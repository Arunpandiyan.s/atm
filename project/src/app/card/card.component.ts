import { Component, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiService } from '../api.service';
import { Card } from '../card.model';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css'],
})
export class CardComponent {
  popup = false;

  cardDetails: Card;
  @ViewChild('cardNo') cardNo!: ElementRef;
  @ViewChild('cvv') cvv!: ElementRef;
  @ViewChild('month') month!: ElementRef;
  @ViewChild('year') year!: ElementRef;
  @ViewChild('availableBalance') availableBalance!: ElementRef;

  constructor(
    private http: HttpClient,
    private cardGenerateService: ApiService
  ) {}
  ngOnInit() {
    this.cardDetails = this.cardGenerateService.cardDetails;
    console.log('Card details:', this.cardDetails);
  }
  ngAfterViewInit() {
    console.log('Inside ngAfterViewInit ');
    this.cardNo.nativeElement.value = this.cardDetails.cardNo;
    this.cvv.nativeElement.value = this.cardDetails.cvv;
    this.month.nativeElement.value = this.cardDetails.month;
    this.year.nativeElement.value = this.cardDetails.year;
    this.availableBalance.nativeElement.value =
      this.cardDetails.availableBalance;
  }

  OnCreate(postdata: { Withdrawal: string }) {
    this.http
      .post(
        'https://http-d6945-default-rtdb.firebaseio.com/Balance.json',
        postdata
      )
      .subscribe((Response) => {
        console.table(Response);
      });
  }
}
